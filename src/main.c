#include <stdio.h>
#include <avr/io.h>
#include <util/delay.h>
#include <time.h>
#include <stdlib.h>
#include <avr/pgmspace.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include "hmi_msg.h"
#include "print_helper.h"
#include "cli_microrl.h"
#include "../lib/hd44780_111/hd44780.h"
#include "../lib/andygock_avr-uart/uart.h"
#include "../lib/helius_microrl/microrl.h"
#include "../lib/matejx_avr_lib/mfrc522.h"
#include "../lib/andy_brown_memdebug/memdebug.h"


#define UART_BAUD           9600
#define UART_STATUS_MASK    0x00FF

#define BLINK_DELAY_MS  1000
#define LED_RED         PORTA0 // Arduino Mega digital pin 22
#define LED_GREEN       PORTA2 // Arduino Mega digital pin 24

static inline void init_rfid_reader(void)
{
    MFRC522_init();
    PCD_Init();
}

static inline void init_leds(void)
{
    /* RGB LED board set up and off */
    DDRA |= _BV(LED_RED) | _BV(LED_GREEN);
    PORTA &= ~(_BV(LED_RED) | _BV(LED_GREEN));
}

static inline void init_con_uart0(void)
{
    uart0_init(UART_BAUD_SELECT(UART_BAUD, F_CPU));
    uart0_puts_p(name);
    uart0_puts_p(PSTR("\r\n"));
    uart0_puts_p(PSTR("Use backspace to delete entry and enter to confirm.\r\n"));
    uart0_puts_p(PSTR("Arrow key's and del do not work currently.\r\n"));
}


static inline void init_con_uart1(void)
{
    uart1_init(UART_BAUD_SELECT(UART_BAUD, F_CPU));
    uart1_puts_p(PSTR(VER_FW));
    uart1_puts_p(PSTR(VER_LIBC));
    uart1_puts_p(PSTR(VER_GCC));
}

static inline void init_sys_timer(void)
{
    // counter_1 = 0; // Set counter to random number 0x19D5F539 in HEX. Set it to 0 if you want
    TCCR1A = 0;
    TCCR1B = 0;
    TCCR1B |= _BV(WGM12); // Turn on CTC (Clear Timer on Compare)
    TCCR1B |= _BV(CS12); // fCPU/256
    OCR1A = 62549; // Note that it is actually two registers OCR5AH and OCR5AL
    TIMSK1 |= _BV(OCIE1A); // Output Compare A Match Interrupt Enable
}

static inline void print_sys_time(void)
{
    // Note that print frequency depends from big program simulation duration.
    // See heartbeat snippet for how to print system time once per second.
    char iso_time[20] = {0x00};
    struct tm now_tm;
    time_t now = time(NULL);
    gmtime_r(&now, &now_tm);
    isotime_r(&now_tm, iso_time);
    uart1_puts(iso_time);
    uart1_puts_p(PSTR("\r\n"));
}

static inline void simu_big_prog(void)
{
    /* Simulate big program with delay and toggle LED */
    PORTA ^= _BV(LED_RED);
    _delay_ms(BLINK_DELAY_MS);
}

static inline void heartbeat(void)
{
    static time_t prev_time;
    char ascii_buf[11] = {0x00};
    time_t now = time(NULL);

    if (prev_time != now) {
        //Print uptime to uart1
        ltoa(now, ascii_buf, 10);
        uart1_puts_p(PSTR("Uptime: "));
        uart1_puts(ascii_buf);
        uart1_puts_p(PSTR(" s.\r\n"));
        //Toggle LED
        PORTA ^= _BV(LED_GREEN);
        prev_time = now;
    }
}

void main(void)
{
    init_leds();
    init_con_uart0();
    init_con_uart1();
    init_sys_timer();
    init_rfid_reader();
    
    sei();
    
    lcd_init();
    lcd_home();
    lcd_puts_P(name);
    // Create microrl object and pointer on it
    microrl_t rl;
    microrl_t *prl = &rl;
    //Call init with ptr to microrl instance and print callback
    microrl_init(prl, uart0_puts);
    //Set callback for execute
    microrl_set_execute_callback(prl, cli_execute);

    while (1) {
        heartbeat();
        //CLI commands are handled in cli_execute()
        microrl_insert_char(prl, (uart0_getc() & UART_STATUS_MASK));
    }
    
}

ISR(TIMER1_COMPA_vect)
{
    system_tick();
}

