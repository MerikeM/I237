//TODO There are most likely unnecessary includes. Clean up during lab6
#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <avr/pgmspace.h>
#include "../lib/hd44780_111/hd44780.h"
#include "../lib/andygock_avr-uart/uart.h"
#include "../lib/helius_microrl/microrl.h"
#include "../lib/matejx_avr_lib/mfrc522.h"
#include "../lib/andy_brown_memdebug/memdebug.h"
#include "hmi_msg.h"
#include "cli_microrl.h"
#include "print_helper.h"

#define NUM_ELEMS(x)        (sizeof(x) / sizeof((x)[0]))

void cli_print_help(const char *const *argv);
void cli_example(const char *const *argv);
void cli_print_ver(const char *const *argv);
void cli_print_ascii_tbls(const char *const *argv);
void cli_handle_number(const char *const *argv);
void cli_rfid_read(const char *const *argv);
void cli_add_card(const char *const *argv);
void cli_print_cards(const char *const *argv);
void cli_remove_card(const char *const *argv);
void cli_print_cmd_error(void);
void cli_print_cmd_arg_error(void);


typedef struct cli_cmd {
    PGM_P cmd;
    PGM_P help;
    void (*func_p)();
    const uint8_t func_argc;
} cli_cmd_t;

typedef struct card {
    uint8_t *uid;
    size_t size;
    char *name;
    struct card *next;
} card;

card *card_ptr;

const char help_cmd[] PROGMEM = "help";
const char help_help[] PROGMEM = "Get help";
const char example_cmd[] PROGMEM = "example";
const char example_help[] PROGMEM =
    "Prints out all provided 3 arguments Usage: example <argument> <argument> <argument>";
const char ver_cmd[] PROGMEM = "version";
const char ver_help[] PROGMEM = "Print FW version";
const char ascii_cmd[] PROGMEM = "ascii";
const char ascii_help[] PROGMEM = "Print ASCII tables";
const char number_cmd[] PROGMEM = "number";
const char number_help[] PROGMEM =
    "Print and display matching number Usage: number <decimal number>";
const char read_cmd[] PROGMEM = "read";
const char read_help[] PROGMEM = "Read the card number";
const char add_cmd[] PROGMEM = "add";
const char add_help[] PROGMEM = "Add Micare card to list. Usage: add <card uid in HEX> <card holder name>";
const char print_cmd[] PROGMEM = "print";
const char print_help[] PROGMEM = "Print list of access cards";
const char rm_cmd[] PROGMEM = "rm";
const char rm_help[] PROGMEM = "Remove Mifare card from list. Usage: rm <card uid in HEX>";


const cli_cmd_t cli_cmds[] = {
    {help_cmd, help_help, cli_print_help, 0},
    {ver_cmd, ver_help, cli_print_ver, 0},
    {example_cmd, example_help, cli_example, 3},
    {ascii_cmd, ascii_help, cli_print_ascii_tbls, 0},
    {number_cmd, number_help, cli_handle_number, 1},
    {read_cmd, read_help, cli_rfid_read, 0},
    {add_cmd, add_help, cli_add_card, 2},
    {print_cmd, print_help, cli_print_cards, 0},
    {rm_cmd, rm_help, cli_remove_card, 1},
};


void cli_print_help(const char *const *argv)
{
    (void) argv;
    uart0_puts_p(PSTR("Implemented commands:\r\n"));

    for (uint8_t i = 0; i < NUM_ELEMS(cli_cmds); i++) {
        uart0_puts_p(cli_cmds[i].cmd);
        uart0_puts_p(PSTR(" : "));
        uart0_puts_p(cli_cmds[i].help);
        uart0_puts_p(PSTR("\r\n"));
    }
}

void cli_example(const char *const *argv)
{
    uart0_puts_p(PSTR("Command had following arguments:\r\n"));

    for (uint8_t i = 1; i < 4; i++) {
        uart0_puts(argv[i]);
        uart0_puts_p(PSTR("\r\n"));
    }
}

void cli_print_ver(const char *const *argv)
{
    (void) argv;
    uart0_puts_p(PSTR(VER_FW));
    uart0_puts_p(PSTR(VER_LIBC));
    uart0_puts_p(PSTR(VER_GCC));
    uart0_puts_p(PSTR("\r\n"));
}


void cli_print_ascii_tbls(const char *const *argv)
{
    (void) argv;
    print_ascii_tbl();
    unsigned char ascii[128];

    for (unsigned char c = 0; c < 128; c++) {
        ascii[c] = c;
    }

    print_for_human(ascii, 128);
}


void cli_handle_number(const char *const *argv)
{
    for (size_t i = 0; i < strlen(argv[1]); i++) {
        if (!isdigit(argv[1][i])) {
            uart0_puts_p(PSTR("Argument is not a decimal number!\r\n"));
            lcd_clr(LCD_ROW_2_START, LCD_VISIBLE_COLS);
            lcd_goto(LCD_ROW_2_START);
            lcd_puts_P(PSTR("Not a number"));
            return;
        }
    }

    int input = atoi(argv[1]);

    if (input >= 0 && input < 10) {
        uart0_puts_p(PSTR("You entered number "));
        uart0_puts_p((PGM_P)pgm_read_word(&(numbers[input])));
        uart0_puts_p(PSTR("\r\n"));
        lcd_clr(LCD_ROW_2_START, LCD_VISIBLE_COLS);
        lcd_goto(LCD_ROW_2_START);
        lcd_puts_P((PGM_P)pgm_read_word(&(numbers[input])));
    } else {
        uart0_puts_p(PSTR("Please enter number between 0 and 9!\r\n"));
        lcd_clr(LCD_ROW_2_START, LCD_VISIBLE_COLS);
        lcd_goto(LCD_ROW_2_START);
        lcd_puts_P(PSTR("Not valid number"));
    }
}

void cli_rfid_read(const char *const *argv)
{
    (void) argv;
    Uid uid;
    Uid *uid_ptr = &uid;
    uart1_puts_p(PSTR("\r\n"));
    if (PICC_IsNewCardPresent()) {
        uart0_puts_p(PSTR("Card selected!\r\n"));
        PICC_ReadCardSerial(uid_ptr);
        uart0_puts_p(PSTR("Card type: "));
        uart0_puts(PICC_GetTypeName(PICC_GetType(uid.sak)));
        uart0_puts_p(PSTR("\r\n"));
        uart0_puts_p(PSTR("Card UID: "));
        for (byte i = 0; i < uid.size; i++) {
            print_byte_in_hex(uid.uidByte[i]);
        }
        uart0_puts_p(PSTR(" (size: "));
        print_byte_in_hex(uid.size);
        uart0_puts_p(PSTR(" bytes)"));
        uart0_puts_p(PSTR("\r\n"));
    } else {
        uart0_puts_p((PSTR("Unable to select card.\r\n")));
    }
}

void cli_add_card(const char *const *argv)
{
    //card info
    struct card *new_card;
    new_card = (card *)malloc(sizeof(card));
    new_card->size = strlen(argv[1]) / 2;
    new_card->uid = (uint8_t *)malloc(new_card->size * sizeof(uint8_t));
    new_card->name = malloc(sizeof(char) * strlen(argv[2]));
    strcpy(new_card->name,argv[2]);
    tallymarker_hextobin(argv[1], new_card->uid, new_card->size);
    
    //check UID length
    if (new_card->size > 10){
        uart0_puts_p(PSTR("UID too long. Max length 10 bytes. \r\n"));
        return;
    }
    
    //check if card is already in list
    struct card *c = card_ptr;
    while (c != NULL) {
        if (c->size == new_card->size){
            bool same = true;
            for (size_t i = 0; i < c->size; i++){
                if (new_card->uid[i] != c->uid[i]) {
                    same = false;
                }
            }
            if (same) {
                uart0_puts_p(PSTR("Can not add card. UID already in list \r\n"));
                return;
            }   
        }
        c = c->next;
    }
    
    //add card to list
    new_card->next = card_ptr;
    card_ptr = new_card;
    
    //print card info
    uart0_puts_p(PSTR("Added card. UID: "));
    for (size_t i = 0; i < card_ptr->size; i++) {
        print_byte_in_hex(card_ptr->uid[i]);
    }
    uart0_puts_p(PSTR(" Size: "));
    print_byte_in_hex(card_ptr->size);
    uart0_puts_p(PSTR(" Holder name: "));
    uart0_puts(card_ptr->name);
    uart0_puts_p(PSTR("\r\n"));
}

void cli_print_cards(const char *const *argv)
{
    (void) argv;
    struct card *c = card_ptr;
    if (c == NULL) {
        uart0_puts_p(PSTR("Card list is empty \r\n"));
    }
    while (c != NULL){
        uart0_puts_p(PSTR("UID["));
        print_byte_in_hex(c->size);
        uart0_puts_p(PSTR("]: "));
        for (size_t i = 0; i < c->size; i++) {
            print_byte_in_hex(c->uid[i]);
        }
        uart0_puts_p(PSTR(" holder name: "));
        uart0_puts(c->name);
        uart0_puts_p(PSTR("\r\n"));
        c = c->next;
    }
}

void cli_remove_card(const char *const *argv)
{
    size_t rm_uid_size = strlen(argv[1]) / 2;
    uint8_t *rm_uid = (uint8_t *)malloc(rm_uid_size * sizeof(uint8_t));    
    tallymarker_hextobin(argv[1], rm_uid, rm_uid_size);
    struct card *c = card_ptr;
    struct card *last = card_ptr;
    while (c != NULL){ //search from all cards
        if (c->size == rm_uid_size){
            bool same = true;
            for(size_t i = 0; i < rm_uid_size; i++) {
                if (c->uid[i] != rm_uid[i]){
                    same = false;
                    break;
                }
            }
            if (same) { //match found, remove card and return from function
                last->next = c->next;
                uart0_puts_p(PSTR("Removed card UID: "));
                for(size_t i = 0; i < rm_uid_size; i++) {
                    print_byte_in_hex(rm_uid[i]);
                }
                uart0_puts_p(PSTR("\r\n"));
                return;
            }
        }
        last = c;
        c=c->next;
    }
    uart0_puts_p(PSTR("No card with this UID in list \r\n"));
}

void cli_print_cmd_error(void)
{
    uart0_puts_p(PSTR("Command not implemented.\r\n\tUse <help> to get help.\r\n"));
}


void cli_print_cmd_arg_error(void)
{
    uart0_puts_p(
        PSTR("To few or too many arguments for this command\r\n\tUse <help>\r\n"));
}


int cli_execute(int argc, const char *const *argv)
{
    // Move cursor to new line. Then user can see what was entered.
    uart0_puts_p(PSTR("\r\n"));

    for (uint8_t i = 0; i < NUM_ELEMS(cli_cmds); i++) {
        if (!strcmp_P(argv[0], cli_cmds[i].cmd)) {
            // Test do we have correct arguments to run command
            // Function arguments count shall be defined in struct
            if ((argc - 1) != cli_cmds[i].func_argc) {
                cli_print_cmd_arg_error();
                return 0;
            }

            // Hand argv over to function via function pointer,
            // cross fingers and hope that funcion handles it properly
            cli_cmds[i].func_p (argv);
            return 0;
        }
    }

    cli_print_cmd_error();
    return 0;
}

