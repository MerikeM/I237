#ifndef _HMI_MSG_H_
#define _HMI_MSG_H_

#define VER_FW "Version: " FW_VERSION " built on: " __DATE__" "__TIME__"\r\n"
#define VER_LIBC "avr-libc version: " __AVR_LIBC_VERSION_STRING__" "
#define VER_GCC "avr-gcc version: " __VERSION__"\r\n"

extern const char name[];
extern PGM_P const numbers[];

#endif
