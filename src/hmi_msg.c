#include <avr/pgmspace.h>

const char name[] PROGMEM = "Merike Meizner";

char const nr_0[] PROGMEM = "Zero";
char const nr_1[] PROGMEM = "One";
char const nr_2[] PROGMEM = "Two";
char const nr_3[] PROGMEM = "Three";
char const nr_4[] PROGMEM = "Four";
char const nr_5[] PROGMEM = "Five";
char const nr_6[] PROGMEM = "Six";
char const nr_7[] PROGMEM = "Seven";
char const nr_8[] PROGMEM = "Eight";
char const nr_9[] PROGMEM = "Nine";

PGM_P const numbers[] PROGMEM = {nr_0, nr_1, nr_2, nr_3, nr_4, nr_5, nr_6, nr_7, nr_8, nr_9};
